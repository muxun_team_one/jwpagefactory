<?php
/**
 * @author       JoomWorker
 * @email        info@joomla.work
 * @url          http://www.joomla.work
 * @copyright    Copyright (c) 2010 - 2019 JoomWorker
 * @license      GNU General Public License version 2 or later
 * @date         2019/01/01 09:30
 */
//no direct accees
defined ('_JEXEC') or die ('Restricted access');

$options = $displayData['options'];
$custom_class  = (isset($options->class)) ? ' ' . $options->class : '';
$data_attr = '';
$doc = JFactory::getDocument();

// Responsive
if(isset($options->sm_col) && $options->sm_col) {
	$options->cssClassName .= ' jwpf-' . $options->sm_col;
}

if(isset($options->xs_col) && $options->xs_col) {
	$options->cssClassName .= ' jwpf-' . $options->xs_col;
}

// Visibility
if(isset($options->hidden_md) && $options->hidden_md) {
	$custom_class .= ' jwpf-hidden-md jwpf-hidden-lg';
}

if(isset($options->hidden_sm) && $options->hidden_sm) {
	$custom_class .= ' jwpf-hidden-sm';
}

if(isset($options->hidden_xs) && $options->hidden_xs) {
	$custom_class .= ' jwpf-hidden-xs';
}

if(isset($options->items_align_center) && $options->items_align_center) {
	$custom_class .= ' jwpf-align-items-center';
}

// Animation
if(isset($options->animation) && $options->animation) {

	$custom_class .= ' jwpf-wow ' . $options->animation;

	if(isset($options->animationduration) && $options->animationduration) {
		$data_attr .= ' data-jwpf-wow-duration="' . $options->animationduration . 'ms"';
	}

	if(isset($options->animationdelay) && $options->animationdelay) {
		$data_attr .= ' data-jwpf-wow-delay="' . $options->animationdelay . 'ms"';
	}
}

$html  = '';
$html .= '<div class="jwpf-' . $options->cssClassName . '" id="column-wrap-id-'. $options->dynamicId .'">';
$html .= '<div id="column-id-'. $options->dynamicId .'" class="jwpf-column' . $custom_class . '" ' . $data_attr . '>';

if (isset($options->background_image) && $options->background_image) {
	if (isset($options->overlay_type) && $options->overlay_type !== 'overlay_none') {
		$html .= '<div class="jwpf-column-overlay"></div>';
	}
}

$html .= '<div class="jwpf-column-addons">';

echo $html;
