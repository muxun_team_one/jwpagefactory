<?php
/**
 * @author       JoomWorker
 * @email        info@joomla.work
 * @url          http://www.joomla.work
 * @copyright    Copyright (c) 2010 - 2019 JoomWorker
 * @license      GNU General Public License version 2 or later
 * @date         2019/01/01 09:30
 */
//no direct accees
defined ('_JEXEC') or die ('Restricted access');

class JwpagefactoryAddonImage extends JwpagefactoryAddons{

	public function render() {
		$settings = $this->addon->settings;
		$class = (isset($settings->class) && $settings->class) ? $settings->class : '';
		$title = (isset($settings->title) && $settings->title) ? $settings->title : '';
		$title_position = (isset($settings->title_position) && $settings->title_position) ? $settings->title_position : 'top';
		$heading_selector = (isset($settings->heading_selector) && $settings->heading_selector) ? $settings->heading_selector : 'h3';

		//Options
		$image = (isset($settings->image) && $settings->image) ? $settings->image : '';
		$alt_text = (isset($settings->alt_text) && $settings->alt_text) ? $settings->alt_text : '';
		$position = (isset($settings->position) && $settings->position) ? $settings->position : '';
		$link = (isset($settings->link) && $settings->link) ? $settings->link : '';
		$target = (isset($settings->target) && $settings->target) ? 'target="' . $settings->target . '"' : '';
		$open_lightbox = (isset($settings->open_lightbox) && $settings->open_lightbox) ? $settings->open_lightbox : 0;
		$image_overlay = (isset($settings->overlay_color) && $settings->overlay_color) ? 1 : 0;

		$output = '';

		if($image) {
			$output  .= '<div class="jwpf-addon jwpf-addon-single-image ' . $position . ' ' . $class . '">';
			$output .= ($title && $title_position != 'bottom') ? '<'.$heading_selector.' class="jwpf-addon-title">' . $title . '</'.$heading_selector.'>' : '';
			$output .= '<div class="jwpf-addon-content">';
			$output .= '<div class="jwpf-addon-single-image-container">';

			if (empty($alt_text)) {
				if (!empty($title)) {
					$alt_text = $title;
				} else {
					$alt_text = basename($image);
				}
			}

			if($image_overlay && $open_lightbox) {
				$output .= '<div class="jwpf-addon-image-overlay">';
				$output .= '</div>';
			}

			if($open_lightbox) {
				$output .= '<a class="jwpf-magnific-popup jwpf-addon-image-overlay-icon" data-popup_type="image" data-mainclass="mfp-no-margins mfp-with-zoom" href="' . $image . '">+</a>';
			}

			if(!$open_lightbox) {
				$output .= ($link) ? '<a ' . $target . ' href="' . $link . '">' : '';
			}

			$output  .= '<img class="jwpf-img-responsive" src="' . $image . '" alt="'. $alt_text .'" title="'.$title.'">';

			if(!$open_lightbox) {
				$output .= ($link) ? '</a>' : '';
			}

			$output  .= '</div>';
			$output .= ($title && $title_position == 'bottom') ? '<'.$heading_selector.' class="jwpf-addon-title">' . $title . '</'.$heading_selector.'>' : '';
			$output  .= '</div>';
			$output  .= '</div>';
		}

		return $output;
	}

	public function scripts() {
		return array(JURI::base(true) . '/components/com_jwpagefactory/assets/js/jquery.magnific-popup.min.js');
	}

	public function stylesheets() {
		return array(JURI::base(true) . '/components/com_jwpagefactory/assets/css/magnific-popup.css');
	}

	public function css() {
		$addon_id = '#jwpf-addon-' . $this->addon->id;
		$settings = $this->addon->settings;
		$open_lightbox = (isset($settings->open_lightbox) && $settings->open_lightbox) ? $settings->open_lightbox : 0;
		$style = (isset($settings->overlay_color) && $settings->overlay_color) ? 'background-color: ' . $settings->overlay_color . ';' : '';
		$style_img = (isset($settings->border_radius) && $settings->border_radius) ? 'border-radius: ' . $settings->border_radius . 'px;' : '';
		$title_padding = (isset($settings->title_padding) && $settings->title_padding) ? $settings->title_padding : '';
		$title_padding_sm = (isset($settings->title_padding_sm) && $settings->title_padding_sm) ? $settings->title_padding_sm : '';
		$title_padding_xs = (isset($settings->title_padding_xs) && $settings->title_padding_xs) ? $settings->title_padding_xs : '';

		$css = '';
		if($open_lightbox && $style) {
			$css .= $addon_id . ' .jwpf-addon-image-overlay{' . $style . '}';
		}

		$css .= $addon_id . ' img{' . $style_img . '}';
		if($title_padding){
			$css .= $addon_id . ' .jwpf-addon-title{padding: ' . $title_padding . '}';
		}
		if($title_padding_sm){
			$css .= '@media (min-width: 768px) and (max-width: 991px) {' .$addon_id. ' .jwpf-addon-title{padding: ' .$title_padding_sm . '}}';
		}
		if($title_padding_xs){
			$css .= '@media (max-width: 767px) {' .$addon_id. ' .jwpf-addon-title{padding: ' . $title_padding_xs . '}}';
		}

		return $css;

	}

	public static function getTemplate() {
		$output = '
		<#
			var image_overlay = 0;
			if(!_.isEmpty(data.overlay_color)){
				image_overlay = 1;
			}
			var open_lightbox = parseInt(data.open_lightbox);
			var title_font_style = data.title_fontstyle || "";

			var alt_text = data.alt_text;

			if(_.isEmpty(alt_text)){
				if(!_.isEmpty(data.title)){
					alt_text = data.title;
				}
			}
		#>
		<style type="text/css">
			<# if(open_lightbox && data.overlay_color){ #>
				#jwpf-addon-{{ data.id }} .jwpf-addon-image-overlay{
					background-color: {{ data.overlay_color }};
				}
			<# } #>

			#jwpf-addon-{{ data.id }} img{
				border-radius: {{ data.border_radius }}px;
			}
			#jwpf-addon-{{ data.id }} .jwpf-addon-title{
				<# if(_.isObject(data.title_padding)) { #>
					padding:{{data.title_padding.md}};
				<# } else { #>
					padding:{{data.title_padding}};
				<# } #>
			}
			@media (min-width: 768px) and (max-width: 991px) {
				<# if(_.isObject(data.title_padding)) { #>
					#jwpf-addon-{{ data.id }} .jwpf-addon-title{
						padding: {{data.title_padding.sm}};
					}
				<# } #>
			}
			@media (max-width: 767px) {
				<# if(_.isObject(data.title_padding)) { #>
					#jwpf-addon-{{ data.id }} .jwpf-addon-title{
						padding: {{data.title_padding.xs}};
					}
				<# } #>
			}
		</style>
		<# if(data.image){ #>
			<div class="jwpf-addon jwpf-addon-single-image {{ data.position }} {{ data.class }}">
				<# if( !_.isEmpty( data.title ) && data.title_position != "bottom" ){ #><{{ data.heading_selector }} class="jwpf-addon-title jw-inline-editable-element" data-id={{data.id}} data-fieldName="title" contenteditable="true">{{ data.title }}</{{ data.heading_selector }}><# } #>
				<div class="jwpf-addon-content">
					<div class="jwpf-addon-single-image-container">
						<# if(image_overlay && open_lightbox) { #>
							<div class="jwpf-addon-image-overlay"></div>
						<# } #>
						<# if(open_lightbox) { #>
							<a class="jwpf-magnific-popup jwpf-addon-image-overlay-icon" data-popup_type="image" data-mainclass="mfp-no-margins mfp-with-zoom" href=\'{{ data.image }}\'>+</a>
						<# } #>
			
						<# if(!open_lightbox) { #>
							<a target="{{ data.target }}" href=\'{{ data.link }}\'>
						<# } #>

						<# if(data.image.indexOf("http://") == -1 && data.image.indexOf("https://") == -1){ #>
							<img class="jwpf-img-responsive" src=\'{{ pagefactory_base + data.image }}\' alt="{{ alt_text }}" title="{{ data.title }}">
						<# } else { #>
							<img class="jwpf-img-responsive" src=\'{{ data.image }}\' alt="{{ alt_text }}" title="{{ data.title }}">
						<# } #>

						<# if(!open_lightbox) { #>
							</a>
						<# } #>

					</div>
					<# if( !_.isEmpty( data.title ) && data.title_position == "bottom" ){ #><{{ data.heading_selector }} class="jwpf-addon-title">{{ data.title }}</{{ data.heading_selector }}><# } #>
				</div>
			</div>
		<# } #>
		';

		return $output;
	}

}
