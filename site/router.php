<?php
/**
 * @author       JoomWorker
 * @email        info@joomla.work
 * @url          http://www.joomla.work
 * @copyright    Copyright (c) 2010 - 2019 JoomWorker
 * @license      GNU General Public License version 2 or later
 * @date         2019/01/01 09:30
 */
//no direct accees
defined('_JEXEC') or die ('Restricted access');

class JwpagefactoryRouter extends JComponentRouterBase
{

	public function build(&$query)
	{
		$app = JFactory::getApplication();
		$menu = $app->getMenu();

		$segments = array();

		if (empty($query['Itemid'])) {
			$menuItem = $menu->getActive();
			$menuItemGiven = false;
		} else {
			$menuItem = $menu->getItem($query['Itemid']);
			$menuItemGiven = true;
		}

		// Check again
		if ($menuItemGiven && isset($menuItem) && $menuItem->component != 'com_jwpagefactory') {
			$menuItemGiven = false;
			unset($query['Itemid']);
		}

		if (isset($query['view'])) {
			$view = $query['view'];
		} else {
			return $segments;
		}

		if (($menuItem instanceof stdClass) && $menuItem->query['view'] == $query['view']) {

			if (!$menuItemGiven) {
				$segments[] = $view;
			}

			unset($query['view']);
		}

		// Page
		if (($view == 'page')) {

			if (isset($query['id']) && $query['id']) {
				if (!isset($query['Itemid']) || empty($query['Itemid']) || (isset($menuItem) && $menuItem->query['id'] != $query['id'])) {
					$id = $this->getPageSegment($query['id']);
					$segments[] = str_replace(':', '-', $id);
				}
				unset($query['id']);
			}

			unset($query['view']);
		}

		// Form
		if (($view == 'form')) {

			if (isset($query['id']) && $query['id']) {
				$id = $this->getPageSegment($query['id']);
				$segments[] = str_replace(':', '-', $id);
				unset($query['id']);
			}

			if (isset($query['layout']) && $query['layout']) {
				$segments[] = $query['layout'];
				unset($query['layout']);
			}

			if (isset($query['tmpl']) && $query['tmpl']) {
				unset($query['tmpl']);
			}

			unset($query['view']);
		}

		return $segments;
	}

	// Parse
	public function parse(&$segments)
	{
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$item = $menu->getActive();
		$total = count((array)$segments);
		$vars = array();
		$view = (isset($item->query['view']) && $item->query['view']) ? $item->query['view'] : 'page';

		if ($view == 'page') {
			if ($total == 2) {
				if ($segments[1] == 'edit') {
					$vars['view'] = 'form';
					$vars['id'] = (int)$segments[0];
					$vars['tmpl'] = 'component';
					$vars['layout'] = 'edit';
				} else {
					$vars['view'] = 'page';
					$vars['id'] = (int)$segments[0];
				}
			}

			if ($total == 1) {
				$vars['view'] = 'page';
				$vars['id'] = (int)$segments[0];
			}
		}

		return $vars;
	}

	private function getPageSegment($id)
	{
		if (!strpos($id, ':')) {
			$db = JFactory::getDbo();
			$dbquery = $db->getQuery(true);
			$dbquery->select($dbquery->qn('title'))
				->from($dbquery->qn('#__jwpagefactory'))
				->where('id = ' . $dbquery->q($id));
			$db->setQuery($dbquery);

			$id .= ':' . JFilterOutput::stringURLSafe($db->loadResult());
		}

		return $id;
	}
}

function JwpagefactoryBuildRoute(&$query)
{
	$router = new JwpagefactoryRouter;
	return $router->build($query);
}

function JwpagefactoryParseRoute($segments)
{
	$router = new JwpagefactoryRouter;
	return $router->parse($query);
}
